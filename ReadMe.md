﻿Arrow Overview 
==============
It has never been so easy, to shoot blues.
#### Version: 2.0(Rhea)

----
## How to use
###Install
1. [Download](https://bitbucket.org/Mr_Mint/arrow-overview/get/master.zip)
2. Extract to Users\%userprofile%\Documents\EVE\Overview (Create overview folder if it doesn't exist)

####Import (& remove old settings)

1. In-game, overview settings -> Misc -> Reset All Overview Settings
2. import overview settings, ArrowOverview

####Import (& keep old settings)

1. In-game, import overview settings, ArrowOverview

----
##Want to contribute?
Fork the repo and create pull requests!